package pokerclient;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JDialog;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;

public class ClientPanel extends JPanel {
	
	private static final long serialVersionUID = 1;
	/**** GUI ****/
	//instantiate menu bar
	private JMenuBar menubar;
	//instantiate scroll pane for menu bar
	private JScrollPane jsp;
	//Elements of the client panel
	//The panel will switch between these
	private LoginMenu loginMenu;
	private CreateAccountPanel createAccountPanel;
	private LocalOnlinePanel localOnlinePanel;
	private SelectOpponentsPanel selectOpponentsPanel;
	private HostJoinPanel hostJoinPanel;
	private WaitingForHostPanel waitingForHostPanel;
	private WaitingForOthersPanel waitingForOthersPanel;
	private GameTablePanel gameTablePanel;
	

	//these enclosed brackets are to show creation/adding of login menu
	{
		/****************** MENUBAR STUFF *******************/
		menubar = new JMenuBar();
		jsp = new JScrollPane();
		
		//JDialog for help menu
		JDialog helpFrame = new JDialog();
		helpFrame.setSize(250, 300);
		helpFrame.setLocation(100,100);
		helpFrame.setModal(true);
	
		//sample string for whatever we want to write into help menu
		String helpString = "Texas Hold'Em!\n\nStarting The Game:\n\n\tCreate Account/Login'\n\n\t"
				+ "Local/Online\n";
		
		JMenuItem helpMenu = new JMenuItem("Help");
		
		//create text area to put string into
		JTextArea helpTime = new JTextArea(helpString);
		helpTime.setEnabled(false);
		helpTime.setOpaque(false);
		helpTime.setDisabledTextColor(Color.BLACK);
		helpMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_H, ActionEvent.SHIFT_MASK));
		
		//instantiate helpPanel
		JPanel helpPanel= new JPanel();
		helpPanel.setLayout(new BorderLayout());
		
		helpFrame.setMinimumSize(new Dimension(200,100));
		helpFrame.setMaximumSize(new Dimension(200,100));
		
		//creating scroll pane for help menu
		jsp = new JScrollPane(helpTime, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		jsp.setOpaque(false);
		jsp.getViewport().setOpaque(false);
		helpPanel.add(jsp);
		
		//action listener when help is clicked
		helpMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				helpFrame.setVisible(true);
				
			}
		});
		
		menubar.add(helpMenu);
		helpFrame.add(helpPanel);
		/*********************************************************/
		
		//Paramaters: loginAction, createAccountAction, guestAction
		loginMenu = new LoginMenu(
			//loginAction
			new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae1) {
				//We first need to check if the username
				//and password are valid before we proceed
				//*See loginMenu methods*/
				//If both are valid swap to the local/online menu
				ClientPanel.this.removeAll();
				ClientPanel.this.add(localOnlinePanel);
				ClientPanel.this.revalidate();
			}},
			//createAccountAction
			new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent ae2){
				ClientPanel.this.removeAll();
				ClientPanel.this.add(createAccountPanel);
				ClientPanel.this.revalidate();
			}},
			//guestAction
			//Note: this needs its own action listener, as we don't 
			//have to check for a valid username/password as we do in
			//the login ActionListener
			new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent ae3){
				ClientPanel.this.removeAll();
				ClientPanel.this.add(localOnlinePanel);
				ClientPanel.this.revalidate();
			}});

		//Set up the panel to display
		setLayout(new BorderLayout());
		add(loginMenu);
		refreshComponents();	
	}
	
	
	private void refreshComponents() {
		//Paramaters: confirmAction
		localOnlinePanel = new LocalOnlinePanel(new ActionListener(){
			//confirmAction
			@Override
			public void actionPerformed(ActionEvent ae){
				ClientPanel.this.removeAll();
				//The next panel to display depends on the option they
				//selected. NOTE: the localOnlinePanel methods aren't implemented 
				if (localOnlinePanel.selectedLocal()) ClientPanel.this.add(selectOpponentsPanel);
				else ClientPanel.this.add(hostJoinPanel);
				ClientPanel.this.revalidate();
			}}
		);
		//Paramaters: cancelAction, confirmAction
		createAccountPanel = new CreateAccountPanel(
			//cancelAction
			new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent ae1){
				ClientPanel.this.removeAll();
				ClientPanel.this.add(loginMenu);
				ClientPanel.this.revalidate();
			}},
			//confirmAction
			new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent ae2){
				ClientPanel.this.removeAll();
				ClientPanel.this.add(localOnlinePanel);
				ClientPanel.this.revalidate();
			}}
		);
		//parameters: confirmAction
		selectOpponentsPanel = new SelectOpponentsPanel(new ActionListener(){
			//confirmAction
			@Override
			public void actionPerformed(ActionEvent ae){
				ClientPanel.this.removeAll();
				ClientPanel.this.add(gameTablePanel);
				ClientPanel.this.revalidate();
			}}
		);
		//parameters: confirmAction
		hostJoinPanel = new HostJoinPanel(new ActionListener(){
			//confirmAction
			@Override
			public void actionPerformed(ActionEvent ae){
				ClientPanel.this.removeAll();
				//Different actions need to be taken depending on whether
				//the user selected host or join
				//*see hostJoinPanel methods*/
				if (hostJoinPanel.selectedJoin()) ClientPanel.this.add(waitingForHostPanel);
				else ClientPanel.this.add(waitingForOthersPanel);
				ClientPanel.this.revalidate();
			}}
		);
		
		//These waiting panels will need to busy wait until they get an OK from the
		//server
		waitingForHostPanel = new WaitingForHostPanel();
		
		waitingForOthersPanel = new WaitingForOthersPanel();
		
		gameTablePanel = new GameTablePanel();
		
	}
}
