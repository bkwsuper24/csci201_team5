package gameserver;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import networkingobjects.FoldObject;
import networkingobjects.NewDeal;
import networkingobjects.PlayerID;

public class PlayerCommunicator implements Runnable{
	private GameServer gameServer;
	private ObjectInputStream in;
	private ObjectOutputStream out;
	private volatile boolean running;
	private int gameID;	//So we can can identify this communicator
	
	public PlayerCommunicator (GameServer gs, Socket s, int i){
		try{
			//A fix so we dont get caught up in construction
			out = new ObjectOutputStream(s.getOutputStream());
			out.flush();
			in = new ObjectInputStream(s.getInputStream());
			//Send the client their id number to facilitate communications on both sides
			out.writeObject(new PlayerID(i));
			out.flush();
			
			this.gameServer = gs;
			this.gameID = i;
		}
		catch(IOException ioe){
			System.out.println("Exception in DatabaseCommunuicator");
		}
	}
	
	@Override
	public void run(){
		try{
			Object o = in.readObject();
			//The player wants to fold
			if (o instanceof PlayerID){
				PlayerID pi = (PlayerID)o;
				gameServer.foldPlayer(pi.id);
			}
		}
		catch(IOException ioe){
			System.out.println("PlayerComm " + ioe.getMessage());
		}
		catch(ClassNotFoundException cnfe){
			System.out.println("PlayerComm " + cnfe.getMessage());
		}
	}
	
	public void notifyOfNewDeal(NewDeal nd){
		try{
			out.writeObject(nd);
			out.flush();
		}
		catch(IOException ioe){
			System.out.println("PlayerComm: new deal");
		}
	}
	
	public void giveTurn(Boolean b){
		try{
			out.writeObject(b);
			out.flush();
		}
		catch(IOException ioe){
			System.out.println("PlayerComm: giveTurn");
		}
	}
	
	public void notifyOfFold(FoldObject fo){
		try{
			out.writeObject(fo);
			out.flush();
		}
		catch(IOException ioe){
			System.out.println("PlayerComm: notifyOfFold");
		}
	}
}
