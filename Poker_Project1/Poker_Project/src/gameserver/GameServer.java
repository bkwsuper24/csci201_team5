package gameserver;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;

import gamemechanics.Card;
import gamemechanics.CardDeck;
import networkingobjects.FoldObject;
import networkingobjects.NewDeal;




/*NOTE: FOR NETWORKING TO WORK SMOOTHLY WE NEED TO ASSIGN ANY 'EMPTY' IDS
 * INSTEAD OF JUST ACCORDING TO THE VECTOR SIZE
 * 
 */
public class GameServer {
	private Vector<PlayerCommunicator> connections;
	
	private CardDeck deck = new CardDeck();
	private Card[] flop;
	private int dealer = -1;
	private int littleBlind = 0;
	private int bigBlind = 1;
	private int playerTurn = 0;
	private Vector<Boolean> stillIn;	//true if the player hasn't folded

	
	public GameServer(int portNumber){
		try{
			//Setup the network
			ServerSocket ss = new ServerSocket(portNumber);
			connections = new Vector<PlayerCommunicator>();
			System.out.println("GameServer: GameServer created");
			//Continuously accept connections. This will allow players to join midgame
			while(true){
				Socket s = ss.accept();
				//Give the user an ID so we can handle the disconnects properly
				int playerID = connections.size(); 
				PlayerCommunicator pc = new PlayerCommunicator(this, s, playerID);
				new Thread(pc).start();
				connections.addElement(pc);
				System.out.println("GameServer: accepted client " + playerID);
				stillIn.addElement(false);
			}
			
		}
		catch(IOException ioe){
			System.out.println("Exception in GameServer");
		}
	}
	
	//Called each time someone collects the pot
	//Reinitialize the game to start a new round
	//Unfold every player, set the pot to 0, etc.
	public void newDeal(){
		//Make a new deck
		deck.shuffleDeck();
		//Make a new flop
		for (int i = 0; i < 5; i++){
			flop[i] = deck.drawCard();
		}
		//Specify a new dealer,littleblind, and big blind
		dealer++;
		while(connections.elementAt(dealer) == null){
			dealer++;
			if(dealer == connections.size()) dealer = 0;
		}
		littleBlind++;
		while(connections.elementAt(littleBlind) == null){
			littleBlind++;
			if(littleBlind == connections.size()) littleBlind = 0;
		}
		bigBlind++;
		while(connections.elementAt(bigBlind) == null){
			bigBlind++;
			if(bigBlind == connections.size()) bigBlind = 0;
		}
		for (int j = 0; j < connections.size(); j++){
			PlayerCommunicator pc = connections.elementAt(j);
			if (pc != null){
				//Update all players to playing
				stillIn.set(j, true);
				//Draw 2 new cards for the player
				Card[] cards = new Card[2];
				cards[0] = deck.drawCard();
				cards[1] = deck.drawCard();
				if (j == dealer){
					pc.notifyOfNewDeal(new NewDeal(flop,cards,true,false,false));	//flop,cards,dealer,lil,big
				}
				else if (j == littleBlind){
					pc.notifyOfNewDeal(new NewDeal(flop,cards,false,true,false));
				}
				else if (j == bigBlind){
					pc.notifyOfNewDeal(new NewDeal(flop,cards,false,false,true));
				}
				else{
					pc.notifyOfNewDeal(new NewDeal(flop,cards,false,false,false));
				}
			}
		}
	}
	
	//Called after any move
	private void nextTurn(){
		playerTurn++;
		while(stillIn.elementAt(playerTurn) == false){
			playerTurn++;
			if(playerTurn == stillIn.size()) playerTurn = 0;
		}
		PlayerCommunicator pc = connections.elementAt(playerTurn);
		if( pc != null){
			pc.giveTurn(new Boolean(true));
		}
		else{
			System.out.println("GameServer: error in nextTurn");
		}
	}
	
	//All possible player actions, signals the end of a turn
	public void foldPlayer(int id){
		stillIn.set(id, false);
		for(int i=0; i<connections.size();i++){
			PlayerCommunicator pc = connections.elementAt(i);
			if(pc != null){
				pc.notifyOfFold(new FoldObject(i));
			}
		}
		nextTurn();
	}
	
}
