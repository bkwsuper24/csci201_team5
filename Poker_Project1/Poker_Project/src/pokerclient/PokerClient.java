package pokerclient;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import gamemechanics.GameConstants;
import gamemechanics.Player;
import gameserver.GameServer;
import networkingobjects.CheckPassword;
import networkingobjects.CheckUsername;
import networkingobjects.DatabaseID;
import networkingobjects.FoldObject;
import networkingobjects.NewDeal;
import networkingobjects.NewUser;
import networkingobjects.PlayerID;
import networkingobjects.PlayerRequest;
import pokerclientgui.PokerClientWindow;

public class PokerClient {
	private static ObjectInputStream databaseIn, gameIn;
	private static ObjectOutputStream databaseOut, gameOut;
	private static String gameServerIPAddress;
	private static int gameServerPortNumber;
	
	private static PokerClientWindow pcw;	//Responsible for all of the client GUI
	private static Player player; 			//The player the client has logged in as
	private static DatabaseID databaseID;
	private static PlayerID playerID;		//The ID for the actual game, facilitates communication with the gameServer
	private static volatile boolean loggedIn = false;
	private static volatile boolean hasSelectedHostOrJoin = false;
	private static boolean choseToHost = false;	//If false, it means they chose to join an existing game

	private static volatile boolean running;	//Whether the user is not at the main menu
	
	public static void main(String[] args) {
		//Create a GUI for the client
		PokerClient.pcw = new PokerClientWindow();
		//Connect to the database
		connectToDatabase(GameConstants.DATABASEIP,GameConstants.DATABASEPORTNUMBER);
		//Busy wait for the database to acknowledge a login
		while(!loggedIn){}
		//At this point the user has logged in and the associated player has been set
		System.out.println("player has logged in");
		//Busy wait until the host has decided to host or hoin a game, 
		while(!hasSelectedHostOrJoin){}
		//Now see what their choice was, and take the necessary action
		//At this point the gameServerIPAdress and gameServerPortNumber have been set
		if(gameServerIPAddress == null){
			setUpGameServer(gameServerPortNumber);
		}
		else{
			joinGameServer(gameServerIPAddress,gameServerPortNumber);
		}
		
		//Need to wait here for the server to give permission to proceed to GUI
		
		//will read input from game here
		while(true){
			try{
				Object o = gameIn.readObject();
				//A new hand will be dealt
				if (o instanceof NewDeal){
					NewDeal nd = (NewDeal)o;
					player.newDeal(nd);	//update the players info
					/*update the gui:
					 * update the dealer, lil, big chips
					 * update the player's cards
					 * update the flop
					 * reset the pot
					 * ALL OF THIS INFO IS CONTAINED IN THE NEW DEAL CLASS
					 */
				}
				//It is this player's turn
				else if (o instanceof Boolean){
					/*ON GAMETABLE GUI:
					 * ENABLE THEIR BUTTONS
					 * (THEY SHOULD BE DISABLED WHEN THEY
					 * END THEIR TURN)
					 */
				}
				else if(o instanceof FoldObject){
					FoldObject fo = (FoldObject)o;
					/*update the gui:
					 * fold object contains an id that refers to which player
					 * to fold on the gui
					 * 
					 */
				}
				
			}
			catch(IOException ioe){
				System.out.println("Exceptio in pokerclient gameIn");
			}
			catch(ClassNotFoundException cnfe){
				System.out.println("Exceptio in pokerclient gameIn cnfe");
			}
		}
	}	
	
	private static void connectToDatabase(String databaseIP, int databasePortNumber){
		try{
			Socket sdb = new Socket(databaseIP,databasePortNumber);
			//Setup in/out stream for database
			databaseOut = new ObjectOutputStream(sdb.getOutputStream());
			databaseOut.flush();
			databaseIn = new ObjectInputStream(sdb.getInputStream());
			//Wait for the database to assign an ID
			Object o = databaseIn.readObject();
			databaseID = (DatabaseID)o;
		}
		catch(IOException ioe){
			System.out.println("Error in PokerClient:ConnectToDatabase");
		}
		catch(ClassNotFoundException cnfe){
			System.out.println("Error in PokerClient:ConnectToDatabase cnfe");
		}
	}
	
	//Called by LoginMenu/CreateAccountPanel
	//Ask the database if this username is registered
	public static boolean checkIfNameExists(String username){
		try{
			databaseOut.writeObject(new CheckUsername(databaseID.id, username, false));	//id, username, name already exists (arbitrary at this point)
			databaseOut.flush();
			System.out.println("PokerClient: sent request to check username");
			//Wait to hear back
			Object response = databaseIn.readObject();
			CheckUsername cu = (CheckUsername)response;
			System.out.println("PokerClient: got response");
			return cu.exists;
		}
		catch (IOException ioe){
			System.out.println(ioe.getMessage());
			return true;
		}
		catch(ClassNotFoundException cnfe){
			System.out.println(cnfe.getMessage());
			return true;
		}
	}
	//Called by LoginMenu
	//Ask the database if this password matches the one on file given the username
	public static boolean checkIfPasswordMatches(String username, String password){
		try{
			databaseOut.writeObject(new CheckPassword(databaseID.id, username, password, false));	//id, username, passsword, password matches (arbitrary at this point)
			databaseOut.flush();
			//Wait to hear back
			Object response = databaseIn.readObject();
			CheckPassword cp = (CheckPassword)response;
			return cp.matches;
		}
		catch (IOException ioe){
			System.out.println(ioe.getMessage());
			return false;
		}
		catch(ClassNotFoundException cnfe){
			System.out.println(cnfe.getMessage());
			return false;
		}
	}
	//Called by CreateAccountPanel
	//Tell the database to add this new user
	public static void addUserToDatabase(String username, String password, String imagePath){
		try{
			//Start a new user out with 5000 chips
			databaseOut.writeObject(new NewUser(username, password, imagePath, 5000, false)); //boolean pertatins to guest
			databaseOut.flush();
		}
		catch (IOException ioe){
			System.out.println(ioe.getMessage());
		}
	}
	
	//Called by loginMenu
	//The username and password are valid, so get the associated player from the database and login
	public static void validCredentials(String username){
		player = getPlayer(username);
		loggedIn = true;
		System.out.println("PokerClient: set the associated player");
	}
	//Called by guestAction. Manually set the player
	public static void setPlayer(Player playa){
		player = playa;
		System.out.println("PokerClient: set the associated player");
	}
	//Get the associated player from the database
	private static Player getPlayer(String username){
		try{
			databaseOut.writeObject(new PlayerRequest(databaseID.id,username));
			databaseOut.flush();
			//Wait to hear back
			Object response = databaseIn.readObject();
			return (Player)response;
		}
		catch (IOException ioe){
			System.out.println(ioe.getMessage());
			return null;
		}
		catch(ClassNotFoundException cnfe){
			System.out.println(cnfe.getMessage());
			return null;
		}
	}
	
	private static void setUpGameServer(int portNumber){
		new GameServer(portNumber);
		//If this client created the server, we know their IPaddress as localhost
		joinGameServer("localhost",portNumber);
	}
	
	public static void joinGameServer(String ipAddress, int portNumber){
		try{
			//Connect to the GameServer
			Socket sgs = new Socket(ipAddress,portNumber);
			//Setup in/out stream for gameplay
			gameOut = new ObjectOutputStream(sgs.getOutputStream());
			gameOut.flush();
			gameIn = new ObjectInputStream(sgs.getInputStream());
			//Wait for the gameserver to assign an ID
			Object o = gameIn.readObject();
			playerID = (PlayerID)o;
		}
		catch(IOException ioe){
			System.out.println("Exception in PokerClient:JoinGameServer");
		}
		catch(ClassNotFoundException cnfe){
			System.out.println("Exception in PokerClient:JoinGameServer cnfe");
		}
	}
	//Called after the user has specified whether they want to host or join
	//and entered a valid ipadress and/or portnumber
	//Whether the user chose to host a game or join
	public static void selectedOnline(String ipAddress, int portNumber){
		gameServerIPAddress = ipAddress;
		gameServerPortNumber = portNumber;
		hasSelectedHostOrJoin = true;
	}
	
	//GAMEPLAY METHODS AVAILABLE TO PLAYER
	public static void folded(){
		try{
			player.fold();
			gameOut.writeObject(playerID);
			gameOut.flush();
			endTurn();
		}
		catch(IOException ioe){
			System.out.println(ioe.getMessage());
		}
	}
	
	//Disable clients button
	private static void endTurn(){
		
	}
	
	

}
