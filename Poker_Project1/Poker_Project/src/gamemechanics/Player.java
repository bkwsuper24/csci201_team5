package gamemechanics;

import java.io.Serializable;

import networkingobjects.NewDeal;

public class Player implements Serializable {
	private static final long serialVersionUID = -1158873880730684415L;
	//Generic, nonvolatile data
	private String name;
	private int money; //cummulative money
	//private Image image;
	private final boolean guest;
	//Game specific, volatile data
	private Card[] cards;	//The players own two cards
	private Card[] flop;	//Makes it easier to deduce hand
	private Card[] hand;	//The hand, consisting of both the player's own cards and the flop
	private boolean dealer;
	private boolean littleBlind;
	private boolean bigBlind;
	private boolean folded;
	private int bet;	//The amount the user has bet THIS BETTING ROUND
	private boolean out;	//If they are out of money
	
	public Player(String name, int money, String imgFilePath, boolean guest){
		this.name = name;
		this.money = money;
		//this.image = img;
		this.guest = guest;
	}
	
	//Generic player info
	public String getName(){ return name;}
	public int getMoney(){ return money;}
//	public Image getProfilePic() { return image;}
	public boolean isGuest() { return guest;};
	
	//Game specific player info
	public void addMoney(int val){
		money += val;
	}
	
	public void deductMoney(int val){
		money -= val;
	}
	
	public Card[] getCards() {
		return cards;
	}
	
	public void setCards(Card[] c){
		cards = c;
	}
	
	public void fold(){
		bet = 0;
		folded = true;
	}
	
	public boolean hasFolded(){
		return folded;
	}
	
	public void unFold(){
		folded = false;
	}
	
	public void call(int toCall){
		if (money < toCall){
			money = 0;
			out = true;
			return;
		}
		money-=toCall;
	}
	
	public void raise(int toCall, int toRaise){
		call(toCall);
		money-=toRaise;
	}
	
	public void newDeal(NewDeal nd){
		folded = false;
		bet = 0;
		cards = nd.cards;
		flop = nd.flop;
		dealer = nd.dealer;
		littleBlind = nd.littleBlind;
		bigBlind = nd.bigBlind;
	}
	
	
}
